/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.demo.config;

import com.google.common.base.Strings;
import com.xxl.job.core.executor.impl.XxlJobSpringExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

/**
 * xxl-job config
 *
 * @author xuxueli 2017-04-28
 */
@Configuration
public class XxlJobConfig implements EnvironmentAware {

    private Logger logger = LoggerFactory.getLogger(XxlJobConfig.class);

    private Environment environment;

    @Bean
    public XxlJobSpringExecutor xxlJobExecutor() {
        logger.info(">>>>>>>>>>> xxl-job config init.");
        XxlJobSpringExecutor xxlJobSpringExecutor = new XxlJobSpringExecutor();
        xxlJobSpringExecutor.setAdminAddresses(environment.getProperty("xxl.job.admin.addresses"));
        xxlJobSpringExecutor.setAppname(environment.getProperty("xxl.job.executor.appname"));
        xxlJobSpringExecutor.setAddress(environment.getProperty("xxl.job.executor.address"));
        xxlJobSpringExecutor.setIp(environment.getProperty("xxl.job.executor.ip"));
        if(!Strings.isNullOrEmpty(environment.getProperty("xxl.job.executor.port"))){
            xxlJobSpringExecutor.setPort(Integer.parseInt(environment.getProperty("xxl.job.executor.port")));
        }
        xxlJobSpringExecutor.setAccessToken(environment.getProperty("xxl.job.accessToken"));
        xxlJobSpringExecutor.setLogPath(environment.getProperty("xxl.job.executor.logpath"));
        if(!Strings.isNullOrEmpty(environment.getProperty("xxl.job.executor.logretentiondays"))){
            xxlJobSpringExecutor.setLogRetentionDays(Integer.parseInt(environment.getProperty("xxl.job.executor.logretentiondays")));
        }
        return xxlJobSpringExecutor;
    }

    /**
     * 针对多网卡、容器内部署等情况，可借助 "spring-cloud-commons" 提供的 "InetUtils" 组件灵活定制注册IP；
     * <p>
     * 1、引入依赖：
     * <dependency>
     * <groupId>org.springframework.cloud</groupId>
     * <artifactId>spring-cloud-commons</artifactId>
     * <version>${version}</version>
     * </dependency>
     * <p>
     * 2、配置文件，或者容器启动变量
     * spring.cloud.inetutils.preferred-networks: 'xxx.xxx.xxx.'
     * <p>
     * 3、获取IP
     * String ip_ = inetUtils.findFirstNonLoopbackHostInfo().getIpAddress();
     */

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

}