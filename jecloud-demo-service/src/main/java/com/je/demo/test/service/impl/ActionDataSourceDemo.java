/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.demo.test.service.impl;

import com.je.common.base.entity.ServiceDataSourceVo;
import com.je.common.base.service.MetaService;
import com.je.common.base.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("actionDataSourceDemo")
public class ActionDataSourceDemo {

    @Autowired
    private MetaService metaService;

    public List<Map<String, Object>> getData(ServiceDataSourceVo serviceDataSourceVo) {
        Map<String, Object> map = serviceDataSourceVo.getParameterMap();
        String AllDataSql = "SELECT KHGL_XXLY_CODE,KHGL_XXLY_NAME ,count(*) as totalCount FROM crm_khgl  GROUP BY KHGL_XXLY_CODE";
        String querySQL = "SELECT KHGL_XXLY_CODE,KHGL_XXLY_NAME ,count(*) as totalCount FROM crm_khgl WHERE KHGL_XXLY_CODE='%s'  GROUP BY KHGL_XXLY_CODE";
        if (StringUtil.isNotEmpty(map.get("KHGL_XXLY_CODE"))) {
            querySQL = String.format(querySQL, map.get("KHGL_XXLY_CODE"));
            return metaService.selectSql(querySQL);
        }
        return metaService.selectSql(AllDataSql);
    }

}
