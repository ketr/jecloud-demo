/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.demo.test.service.impl;

import com.je.workflow.model.EventSubmitDTO;
import com.je.workflow.rpc.dictionary.WorkFlowExecuteCustomMethodService;
import org.apache.servicecomb.provider.pojo.RpcSchema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RpcSchema(schemaId = "myWorkFlowExecuteCustomMethodService")

public class MyWorkFlowExecuteCustomMethodServiceImpl implements WorkFlowExecuteCustomMethodService {

    private static final Logger logger = LoggerFactory.getLogger(MyWorkFlowExecuteCustomMethodServiceImpl.class);

    @Override
    public void executeCustomMethod(EventSubmitDTO eventSubmitDTO) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(String.format("taskId:%s   ,   ", eventSubmitDTO.getTaskId()));
        stringBuffer.append(String.format("currentTaskName:%s   ,   ", eventSubmitDTO.getCurrentTaskName()));
        stringBuffer.append(String.format("targetTaskName:%s   ,   ", eventSubmitDTO.getTargetTaskName()));
        stringBuffer.append(String.format("targetTransition:%s   ,   ", eventSubmitDTO.getTargetTransition()));
        stringBuffer.append(String.format("operatorEnum:%s   ,   ", eventSubmitDTO.getOperatorEnum().toString()));
        stringBuffer.append(String.format("submitComment:%s   ,   ", eventSubmitDTO.getSubmitComment()));
        stringBuffer.append(String.format("assignees:%s   ,   ", eventSubmitDTO.getAssignees()));
        stringBuffer.append(String.format("funcCode:%s   ,   ", eventSubmitDTO.getFuncCode()));
        stringBuffer.append(String.format("dynaBean:%s   ,   ", eventSubmitDTO.getDynaBean().toString()));
        logger.info(stringBuffer.toString());
        System.out.println(eventSubmitDTO);
    }

}