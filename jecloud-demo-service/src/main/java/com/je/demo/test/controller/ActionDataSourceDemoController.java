package com.je.demo.test.controller;

import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.Strings;
import com.je.common.base.entity.ActionDataSourceVo;
import com.je.common.base.service.MetaService;
import com.je.common.base.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/je/demo/actionDataSourceDemo")
public class ActionDataSourceDemoController {

    @Autowired
    private MetaService metaService;

    @ResponseBody
    @RequestMapping(value = "/getDataSource", method = RequestMethod.POST)
    public List<Map<String, Object>> doTest(HttpServletRequest request, ActionDataSourceVo actionDataSourceVo) {
        JSONObject jsonObject = Strings.isNullOrEmpty(actionDataSourceVo.getParameterStr()) ? new JSONObject() : JSONObject.parseObject(StringUtil.deCode(actionDataSourceVo.getParameterStr()));
        String AllDataSql = "SELECT KHGL_XXLY_CODE,KHGL_XXLY_NAME ,count(*) as totalCount FROM crm_khgl  GROUP BY KHGL_XXLY_CODE";
        String querySQL = "SELECT KHGL_XXLY_CODE,KHGL_XXLY_NAME ,count(*) as totalCount FROM crm_khgl WHERE KHGL_XXLY_CODE='%s'  GROUP BY KHGL_XXLY_CODE";
        if (StringUtil.isNotEmpty(jsonObject.getString("KHGL_XXLY_CODE"))) {
            querySQL = String.format(querySQL, jsonObject.get("KHGL_XXLY_CODE"));
            return metaService.selectSql(querySQL);
        }
        return metaService.selectSql(AllDataSql);
    }

}
