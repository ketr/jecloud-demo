/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.demo.test.controller;

import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.DynaBean;
import com.je.common.base.entity.ActionDataSourceVo;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.service.DataSourceRpcService;
import com.je.common.base.util.StringUtil;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/je/test")
public class TestController extends AbstractPlatformController {

    @Autowired
    DataSourceRpcService dataSourceRpcService;

    @ResponseBody
    @RequestMapping(value = "/doTest", method = RequestMethod.POST)
    public List<Map<String,Object>> doTest(HttpServletRequest request, ActionDataSourceVo actionDataSourceVo )  {
        JSONObject jsonObject =  JSONObject.parseObject(StringUtil.deCode(actionDataSourceVo.getParameterStr()));
        return metaService.selectSql("select * from PM_XMLX where XMLX_XMH={0}",jsonObject.getString("XMLX_XMH"));
    }

    @ResponseBody
    @RequestMapping(value = "/doTestTwo", method = RequestMethod.POST)
    public Map<String,Object> doTestTwo(HttpServletRequest request)  {
        List<Map<String,Object>> list = metaService.selectSql("SELECT * FROM je_test_import4");
        Map<String,Object> result = new HashedMap();
        result.put("listData",list);
        result.put("code","200");
        result.put("message","");
        return result;
    }

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("1");
        list.add(null);
        System.out.println(list.size());
    }
}
