package com.je.demo.test.controller;

import com.je.common.base.DynaBean;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.service.CommonService;
import com.je.common.base.service.MetaService;
import com.je.common.base.service.rpc.SystemSettingRpcService;
import com.je.common.base.util.SecurityUserHolder;
import com.je.common.base.util.StringUtil;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;


/**
 * 此类为demo服务下的一个测试类
 * 1、包含增删改查方法接口，供前端调运
 * 2、平台基础操作方法、比如获取当前登录人信息；获取系统设置数据；
 */
@RestController
@RequestMapping(value = "/je/demo")
public class DemoTestController extends AbstractPlatformController {

    /**
     * 新增数据
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/doDemoSave", method = RequestMethod.POST)
    public BaseRespResult doDemoSave(BaseMethodArgument param, HttpServletRequest request) {
        //获取前端传参
        String JKDY_XMMC = getStringParameter(request, "JKDY_XMMC");
        String JKDY_XMZT_CODE = getStringParameter(request, "JKDY_XMZT_CODE");
        String JKDY_XMZT_NAME = getStringParameter(request, "JKDY_XMZT_NAME");
        String JKDY_XMKSSJ = getStringParameter(request, "JKDY_XMKSSJ");
        String JKDY_XMJSSJ = getStringParameter(request, "JKDY_XMJSSJ");
        String JKDY_XMGS = getStringParameter(request, "JKDY_XMGS");
        //编号生成规则
        String codeGenFieldInfo = getStringParameter(request, "codeGenFieldInfo");
        String tableCode = param.getTableCode();
        DynaBean dynaBean = new DynaBean(tableCode, true);
        //设置系统字段默认值,设置sy_系统字段默认值
        commonService.buildModelCreateInfo(dynaBean);
        //构建编号
        if (StringUtil.isNotEmpty(codeGenFieldInfo)) {
            commonService.buildCode(codeGenFieldInfo, dynaBean);
        }
        dynaBean.setStr("JKDY_XMMC", JKDY_XMMC);
        dynaBean.setStr("JKDY_XMZT_CODE", JKDY_XMZT_CODE);
        dynaBean.setStr("JKDY_XMZT_NAME", JKDY_XMZT_NAME);
        dynaBean.setStr("JKDY_XMKSSJ", JKDY_XMKSSJ);
        dynaBean.setStr("JKDY_XMJSSJ", JKDY_XMJSSJ);
        dynaBean.setStr("JKDY_XMGS", JKDY_XMGS);
        metaService.insert(dynaBean);
        return BaseRespResult.successResult(dynaBean);
    }

    /**
     * 根据条件删除数据
     * @param param
     * @param request
     * @return
     */
    @RequestMapping(value = "/doDeleteById", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult doDeleteById(BaseMethodArgument param, HttpServletRequest request) {
        //取得本次数据的主键
        int count = 0;
        String[] Ids = getStringParameter(request, "ids").split(",");
        String tableCode = param.getTableCode();
        for (String id : Ids) {
            count = metaService.delete(tableCode, ConditionsWrapper.builder().eq("JE_DEMO_JKDY_ID", id));
        }
        return BaseRespResult.successResult(String.format("%s 条记录被删除", count));
    }



    /**
     * 根据条件修改数据
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/doDemoUpdate", method = RequestMethod.POST)
    public BaseRespResult doDemoUpdate(BaseMethodArgument param, HttpServletRequest request) {
        //获取主键
        String JE_DEMO_JKDY_ID = getStringParameter(request, "JE_DEMO_JKDY_ID");
        //获取要修改的数据
        String JKDY_XMMC = getStringParameter(request, "JKDY_XMMC");
        String JKDY_XMGS = getStringParameter(request, "JKDY_XMGS");
        //获取表名
        String tableCode = param.getTableCode();
        //根据主键查询查询当前修改数据
        DynaBean dynaBean = metaService.selectOne(tableCode, ConditionsWrapper.builder().eq("JE_DEMO_JKDY_ID", JE_DEMO_JKDY_ID));
        dynaBean.setStr("JKDY_XMMC", JKDY_XMMC);
        dynaBean.setStr("JKDY_XMGS", JKDY_XMGS);
        metaService.update(dynaBean);
        return BaseRespResult.successResult(dynaBean);
    }


    /**
     * 查询数据
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getSelectData", method = RequestMethod.POST)
    public BaseRespResult getSelectData(BaseMethodArgument param, HttpServletRequest request) {
        //获取表名
        String tableCode = param.getTableCode();
        String JKDY_XMMC = getStringParameter(request, "JKDY_XMMC");
        //如果有参数 就根据参数查，如果无参数 就查所有数据
        if (StringUtil.isNotEmpty(JKDY_XMMC)) {
            List<DynaBean> list = metaService.select(tableCode, ConditionsWrapper.builder().eq("JKDY_XMMC", JKDY_XMMC));
            return BaseRespResult.successResult(list);
        }
        List<Map<String, Object>> list1 = metaService.selectSql("SELECT * FROM JE_DEMO_JKDY");
        return BaseRespResult.successResult(list1);
    }


    /**
     * 获取当前用户信息
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getCurrentUser", method = RequestMethod.POST)
    public BaseRespResult getCurrentUser() {
        return BaseRespResult.successResult(SecurityUserHolder.getCurrentAccount());
    }

    /**
     * 获取系统设置的数据
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getSystemSetting", method = RequestMethod.POST)
    public BaseRespResult getSystemSetting() {
        Map<String, String> maplist = systemSettingRpcService.requireAllSettingValue();
        return BaseRespResult.successResult(maplist);
    }
}
