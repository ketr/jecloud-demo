package com.je.demo.project.controller;

import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.demo.project.service.ProjectRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/je/demo/project/role")
public class ProjectRoleController extends AbstractPlatformController {

    @Autowired
    private ProjectRoleService projectRoleService;

    @RequestMapping(value = "/batchImport", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult batchImport(HttpServletRequest request) {
        String projectId = request.getParameter("projectId");
        String roleIds = request.getParameter("roleIds");
        if (Strings.isNullOrEmpty(projectId) || Strings.isNullOrEmpty(roleIds)) {
            return BaseRespResult.errorResult("参数错误！");
        }
        projectRoleService.batchImport(projectId, roleIds);
        return BaseRespResult.successResult("导入成功！");
    }

    @RequestMapping(value = "/initializeProject", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult initializeProject(HttpServletRequest request) {
        String projectId = request.getParameter("projectId");
        //根据业务逻辑，获取哪些角色要初始化
        String roleIds = "";
        projectRoleService.batchImport(projectId, roleIds);
        return BaseRespResult.successResult("导入成功！");
    }

    @RequestMapping(value = "/synchronizeRolePermissions", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult synchronizeRolePermissions(HttpServletRequest request) {
        String roleIds = request.getParameter("roleIds");
        projectRoleService.synchronizeRolePermissions(roleIds);
        return BaseRespResult.successResult("同步成功！");
    }

    @Override
    @RequestMapping(value = "/doSave", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult doSave(BaseMethodArgument param, HttpServletRequest request) {
        return BaseRespResult.successResult(projectRoleService.doSave(param, request).getValues());
    }

    @Override
    @RequestMapping(value = "/doUpdateList", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult doUpdateList(BaseMethodArgument param, HttpServletRequest request) {
        String funcType = param.getParameter("funcType");
        String updatesels = param.getParameter("updatesels");
        if (Strings.isNullOrEmpty(updatesels)) {
            updatesels = "1";
        }

        if (!Strings.isNullOrEmpty(funcType) && !funcType.equals("VIEW") || updatesels.equals("0")) {
            int i = projectRoleService.doUpdateList(param, request);
            return BaseRespResult.successResult(String.format("%s 条记录被更新", i));
        } else {
            List<DynaBean> list = projectRoleService.doUpdateListAndReturn(param, request);
            List<Object> returnList = new ArrayList<>();
            for (DynaBean dynaBean : list) {
                if (dynaBean == null || dynaBean.getValues() == null) {
                    break;
                }
                returnList.add(dynaBean.getValues());
            }
            Long count = Long.valueOf(list.size());
            return new BaseRespResult(returnList, String.format("%s 条记录被更新", count), "操作成功");
        }
    }

    @Override
    @RequestMapping(value = "/doRemove", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult doRemove(BaseMethodArgument param, HttpServletRequest request) {
        return BaseRespResult.successResult(String.format("%s 条记录被删除", projectRoleService.doRemove(param, request)));
    }

}
