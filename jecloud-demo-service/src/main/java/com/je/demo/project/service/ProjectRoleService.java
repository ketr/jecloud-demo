package com.je.demo.project.service;

import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import com.je.common.base.mapper.query.NativeQuery;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.service.CommonService;
import com.je.common.base.service.MetaRbacService;
import com.je.common.base.service.MetaService;
import com.je.common.base.service.rpc.BeanService;
import com.je.common.base.util.JEUUID;
import com.je.common.base.util.StringUtil;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.rbac.model.project.ProjectRole;
import com.je.rbac.rpc.project.ProjectRoleRpcService;
import com.je.servicecomb.CommonRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ProjectRoleService implements CommonRequestService {

    @Autowired
    private BeanService beanService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private MetaService metaService;
    @Autowired
    private MetaRbacService metaRbacService;
    @Autowired
    private ProjectRoleRpcService projectRoleRpcService;

    @Transactional
    public void batchImport(String projectId, String roleIds) {
        DynaBean projectBean = metaService.selectOneByPk("JE_PROJECT_INFO", projectId);
        if (projectBean == null) {
            throw new PlatformException("项目不存在！", PlatformExceptionEnum.UNKOWN_ERROR);
        }
        List<DynaBean> roleBeanList = metaRbacService.selectByTableCodeAndNativeQuery("JE_RBAC_ROLE",
                NativeQuery.build().in("JE_RBAC_ROLE_ID", Arrays.asList(roleIds.split(","))));
        List<DynaBean> insertBeanList = new ArrayList<>();
        DynaBean eachProjectRoleBean;
        for (DynaBean eachRoleBean : roleBeanList) {
            eachProjectRoleBean = new DynaBean("JE_PROJECT_ROLE", false);
            eachProjectRoleBean.set("JE_PROJECT_INFO_ID", projectId);
            eachProjectRoleBean.set("ROLE_NAME", eachRoleBean.getStr("ROLE_NAME"));
            eachProjectRoleBean.set("ROLE_CODE", projectBean.getStr("INFO_CODE") + "_" + eachRoleBean.getStr("ROLE_CODE"));
            eachProjectRoleBean.set("JE_RBAC_ROLE_ID", eachRoleBean.getStr("JE_RBAC_ROLE_ID"));
            eachProjectRoleBean.set("ROLE_REMARK", "批量添加");
            commonService.buildModelCreateInfo(eachProjectRoleBean);
            metaService.insert(eachProjectRoleBean);
            insertBeanList.add(eachProjectRoleBean);
        }
        cascadeRole(insertBeanList);
    }

    @Transactional
    public DynaBean doSave(BaseMethodArgument param, HttpServletRequest request) {
        //获取新增数据
        DynaBean dynaBean = (DynaBean) request.getAttribute("dynaBean");
        //设置系统字段默认值
        commonService.buildModelCreateInfo(dynaBean);
        //构建编号
        String codeGenFieldInfo = getStringParameter(request, "codeGenFieldInfo");
        if (StringUtil.isNotEmpty(codeGenFieldInfo)) {
            commonService.buildCode(codeGenFieldInfo, dynaBean);
        }
        String funcCode = getStringParameter(request, "funcCode");
        dynaBean = commonService.doSave(dynaBean);
        //单根树或多根树处理
        //检测子功能是否增加ROOT节点 (子功能多树)
        commonService.doChildrenTree(dynaBean, funcCode);
        //处理单附件多附件上传
        commonService.doSaveFileMetadata(dynaBean, param.getBatchFilesFields(), param.getUploadableFields(), param.getFuncCode());
        cascadeRole(Arrays.asList(dynaBean));
        return dynaBean;
    }


    @Transactional
    public int doRemove(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean dynaBean = (DynaBean) request.getAttribute("dynaBean");
        if (StringUtil.isEmpty(param.getTableCode()) || StringUtil.isEmpty(param.getIds())) {
            throw new PlatformException("参数错误", PlatformExceptionEnum.UNKOWN_ERROR);
        }

        String[] idArr = param.getIds().split(",");
        metaRbacService.deleteByTableCodeAndNativeQuery("JE_RBAC_ROLE", NativeQuery.build().in("JE_RBAC_ROLE_ID", Arrays.asList(idArr)));

        //级联删除子功能
        commonService.removeChild(param.getFuncCode(), param.getTableCode(), dynaBean.getPkCode(), param.getIds(), param.getDoTree());
        commonService.doRemoveBatchFiles(param.getTableCode(), param.getIds());

        int count = metaService.delete(param.getTableCode(), ConditionsWrapper.builder().in(dynaBean.getPkCode(), idArr));
        return count;
    }

    @Transactional
    public int doUpdateList(BaseMethodArgument param, HttpServletRequest request) {
        String funcType = getStringParameter(request, "funcType");
        String funcCode = getStringParameter(request, "funcCode");
        String updatesels = getStringParameter(request, "updatesels");
        // 构建编号
        String codeGenFieldInfo = param.getCodeGenFieldInfo();
        if (!Strings.isNullOrEmpty(updatesels) && "0".equals(updatesels)) {
            return commonService.doUpdateAllList(param.getTableCode(), funcType, funcCode, ((DynaBean) request.getAttribute("dynaBean")).getValues(), param, request);
        } else {
            List<DynaBean> lists = beanService.buildUpdateList(param.getStrData(), param.getTableCode());
            List<DynaBean> insertRoleList = new ArrayList<>();
            for (DynaBean bean : lists) {
                String action = bean.getStr("__action__");
                if ("doInsert".equals(action)) {
                    insertRoleList.add(bean);
                }
            }
            cascadeRole(insertRoleList);
            List<DynaBean> updateList = commonService.doUpdateList(param.getTableCode(), param.getStrData(), funcType, funcCode, codeGenFieldInfo);
            return updateList.size();
        }
    }

    @Transactional
    public List<DynaBean> doUpdateListAndReturn(BaseMethodArgument param, HttpServletRequest request) {
        String funcType = getStringParameter(request, "funcType");
        String funcCode = getStringParameter(request, "funcCode");
        // 构建编号
        String codeGenFieldInfo = param.getCodeGenFieldInfo();
        List<DynaBean> lists = beanService.buildUpdateList(param.getStrData(), param.getTableCode());
        List<DynaBean> insertRoleList = new ArrayList<>();
        for (DynaBean bean : lists) {
            String action = bean.getStr("__action__");
            if ("doInsert".equals(action)) {
                insertRoleList.add(bean);
            }
        }
        cascadeRole(insertRoleList);
        return commonService.doUpdateList(param.getTableCode(), param.getStrData(), funcType, funcCode, codeGenFieldInfo);
    }

    private void cascadeRole(List<DynaBean> projectRoleBeanList) {
        if (projectRoleBeanList.isEmpty()) {
            return;
        }

        List<ProjectRole> projectRoleList = new ArrayList<>();
        ProjectRole eachProjectRole;
        DynaBean projectInfoBean = null;
        for (DynaBean eachProjectRoleBean : projectRoleBeanList) {
            if (projectInfoBean == null) {
                projectInfoBean = metaService.selectOneByPk("JE_PROJECT_INFO", eachProjectRoleBean.getStr("JE_PROJECT_INFO_ID"));
            }
            eachProjectRole = new ProjectRole();
            eachProjectRole.setProjectId(eachProjectRoleBean.getStr("JE_PROJECT_INFO_ID"));
            eachProjectRole.setProjectCode(projectInfoBean.getStr("INFO_CODE"));
            eachProjectRole.setProjectName(projectInfoBean.getStr("INFO_NAME"));

            eachProjectRole.setRoleId(eachProjectRoleBean.getStr("JE_PROJECT_ROLE_ID"));
            eachProjectRole.setRoleCode(eachProjectRoleBean.getStr("ROLE_CODE"));
            eachProjectRole.setRoleName(eachProjectRoleBean.getStr("ROLE_NAME"));
            eachProjectRole.setBaseRoleId(eachProjectRoleBean.getStr("JE_RBAC_ROLE_ID"));
            projectRoleList.add(eachProjectRole);
        }

        projectRoleRpcService.createProjectRoleList(projectRoleList);

    }

    public void synchronizeRolePermissions(String roleIds) {
        //rbac中的角色信息
        List<DynaBean> roleBeanList = metaRbacService.selectByTableCodeAndNativeQuery("JE_RBAC_ROLE",
                NativeQuery.build().in("JE_RBAC_ROLE_ID", Arrays.asList(roleIds.split(","))));
        for (DynaBean dynaBean : roleBeanList) {
            //模版角色
            String ROLE_TEMPLATEROLE_ID = dynaBean.getStr("ROLE_TEMPLATEROLE_ID");
            if (Strings.isNullOrEmpty(ROLE_TEMPLATEROLE_ID)) {
                continue;
            }
            //当前角色所拥有权限
            List<DynaBean> rolePermBeanList = metaRbacService.selectByTableCodeAndNativeQuery("JE_RBAC_ROLEPERM", NativeQuery.build()
                    .eq("JE_RBAC_ROLE_ID", dynaBean.getStr("JE_RBAC_ROLE_ID")));
            //模版角色权限
            List<DynaBean> templateRolePermBeanList = metaRbacService.selectByTableCodeAndNativeQuery("JE_RBAC_ROLEPERM", NativeQuery.build()
                    .eq("JE_RBAC_ROLE_ID", ROLE_TEMPLATEROLE_ID));

            List<DynaBean> addRolePermBeanList = new ArrayList<>();
            //补全逻辑 使用JE_RBAC_PERM_ID判断是否存在
            // 使用 JE_RBAC_PERM_ID 判断当前角色缺失的权限
            Set<String> currentPermIds = rolePermBeanList.stream()
                    .map(bean -> bean.getStr("JE_RBAC_PERM_ID"))
                    .collect(Collectors.toSet());

            for (DynaBean templatePermBean : templateRolePermBeanList) {
                String permId = templatePermBean.getStr("JE_RBAC_PERM_ID");
                if (!currentPermIds.contains(permId)) {
                    templatePermBean.setStr("JE_RBAC_ROLE_ID", dynaBean.getStr("JE_RBAC_ROLE_ID"));
                    addRolePermBeanList.add(templatePermBean);
                }
            }

            //增量添加
            for (DynaBean rolePermBean : addRolePermBeanList) {
                rolePermBean.set("JE_RBAC_ROLEPERM_ID", JEUUID.uuid());
                rolePermBean.set("JE_RBAC_ROLE_ID", rolePermBean.getStr("JE_RBAC_ROLE_ID"));
                commonService.buildModelCreateInfo(rolePermBean);
                metaRbacService.insertByTableCode("JE_RBAC_ROLEPERM", rolePermBean);
            }

        }

    }
}
