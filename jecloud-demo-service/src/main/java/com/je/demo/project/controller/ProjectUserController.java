package com.je.demo.project.controller;

import com.je.auth.check.annotation.AuthCheckDataPermission;
import com.je.auth.check.annotation.CheckDataType;
import com.je.common.base.document.ContentTypeSuffixEnum;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.util.JEUUID;
import com.je.common.base.util.StringUtil;
import com.je.demo.project.service.ProjectUserService;
import com.je.servicecomb.InputstreamContentTypePart;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.commons.lang3.StringUtils;
import org.apache.servicecomb.common.rest.HttpTransportContext;
import org.apache.servicecomb.foundation.vertx.http.ReadStreamPart;
import org.apache.servicecomb.provider.springmvc.reference.RestTemplateBuilder;
import org.apache.servicecomb.swagger.invocation.context.ContextUtils;
import org.apache.servicecomb.swagger.invocation.context.TransportContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.File;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping(value = "/je/demo/project/user")
public class ProjectUserController extends AbstractPlatformController {

    @Autowired
    private ProjectUserService projectUserService;
    @Autowired
    private Environment environment;

    @RequestMapping(
            value = {"/doSave"},
            method = {RequestMethod.POST},
            produces = {"application/json; charset=utf-8"}
    )
    @AuthCheckDataPermission(
            checkDataType = CheckDataType.UPDATE
    )
    public BaseRespResult doSave(BaseMethodArgument param, HttpServletRequest request) {
        return BaseRespResult.successResult(this.projectUserService.doSave(param, request).getValues());
    }

    @RequestMapping(
            value = {"/doUpdate"},
            method = {RequestMethod.POST},
            produces = {"application/json; charset=utf-8"}
    )
    @AuthCheckDataPermission(
            checkDataType = CheckDataType.UPDATE
    )
    public BaseRespResult doUpdate(BaseMethodArgument param, HttpServletRequest request) {
        return BaseRespResult.successResult(this.projectUserService.doUpdate(param, request).getValues());
    }

}
