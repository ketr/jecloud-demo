package com.je.demo.project.controller;

import com.google.common.collect.Lists;
import com.je.auth.check.annotation.AuthCheckDataPermission;
import com.je.auth.check.annotation.CheckDataType;
import com.je.common.base.DynaBean;
import com.je.common.base.entity.func.FuncInfo;
import com.je.common.base.entity.func.FuncQueryStrategy;
import com.je.common.base.mapper.query.NativeQuery;
import com.je.common.base.mapper.query.Query;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.service.MetaResourceService;
import com.je.common.base.service.PlatformService;
import com.je.common.base.service.QueryBuilderService;
import com.je.common.base.service.rpc.BeanService;
import com.je.common.base.service.rpc.DataSecretRpcServive;
import com.je.common.base.util.ProjectContextHolder;
import com.je.common.base.util.SystemSecretUtil;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.ibatis.extension.plugins.pagination.Page;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/je/demo/project/ceshixiangmu")
public class ProjectAbstractPermController extends AbstractPlatformController {

    @Autowired
    private PlatformService platformService;
    @Autowired
    private MetaResourceService metaResourceService;
    @Autowired
    private BeanService beanService;
    @Autowired
    private QueryBuilderService queryBuilder;
    @Autowired
    private DataSecretRpcServive dataSecretRpcServive;

    @Override
    @RequestMapping(value = "/load", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @AuthCheckDataPermission(checkDataType = CheckDataType.LOAD)
    public BaseRespResult load(BaseMethodArgument param, HttpServletRequest request) {
        if (param.getLimit() == 0) {
            param.setLimit(-1);
        }

        //分页对象
        Page page = new Page<>(param.getPage(), param.getLimit());

        String funcCode = param.getFuncCode();

        //构建查询条件
        ConditionsWrapper wrapper = buildWrapper(param, request);

        List<Map<String, Object>> list = metaService.load(funcCode, page, wrapper);
        page.setRecords(list);
        if (page == null) {
            return BaseRespResult.successResultPage(Lists.newArrayList(), 0L);
        }
        return BaseRespResult.successResultPage(page.getRecords(), (long) page.getTotal());
    }

    public ConditionsWrapper buildWrapper(BaseMethodArgument param, HttpServletRequest request) {
        //功能code，表code
        String funcCode = param.getFuncCode();
        String tableCode = param.getTableCode();

        //获取查询条件
        Query query = param.buildQuery();
        query.setFuncId(param.getFuncId());
        query.setFuncCode(funcCode);

        //设置是否覆盖功能whereSql
        query.setOverrideFuncWhere("1".equals(getStringParameter(request, "coverJquery")));
        query.setSyProductId(getStringParameter(request, "SY_PRODUCT_ID"));

        //功能相关配置
        if (StringUtils.isNotBlank(funcCode)) {
            //获取功能配置
            FuncInfo funcInfo = commonService.functionConfig(funcCode);
            query.setFuncId(funcInfo.getFuncId());
            query.setFuncPkCode(funcInfo.getPkCode());
            query.setFuncWhereSql(funcInfo.getWhereSql());
            query.setFuncOrderSql(funcInfo.getOrderSql());

            //获取查询策略
            if (StringUtils.isNotBlank(query.getStrategyId())) {
                FuncQueryStrategy strategy = null;
                //查找缓存中的查询策略
                if (funcInfo.getStrategies() != null) {
                    List<FuncQueryStrategy> strategiesFilter = funcInfo.getStrategies().stream()
                            .filter(p -> query.getStrategyId().equals(p.getId())).collect(Collectors.toList());
                    if (!strategiesFilter.isEmpty()) {
                        strategy = strategiesFilter.get(0);
                    }
                }
                //数据库查找查询策略
                if (strategy == null) {
                    //7.封装查询策略
                    DynaBean queryStrategy = metaResourceService.selectOneByNativeQuery("JE_CORE_QUERYSTRATEGY", NativeQuery.build().eq("JE_CORE_QUERYSTRATEGY_ID", query.getStrategyId()));
                    if (queryStrategy != null) {
                        strategy = new FuncQueryStrategy(queryStrategy.getStr("JE_CORE_QUERYSTRATEGY_ID"), queryStrategy.getStr("QUERYSTRATEGY_SQL"), queryStrategy.getStr("QUERYSTRATEGY_FGGNSQL"));
                    }
                }
                query.setStrategyBean(strategy);
            }
        }

        ConditionsWrapper wrapper = ConditionsWrapper.builder().table(tableCode);
        appendSecretInfo(wrapper);
        appendProjectInfo(wrapper);

        //设置条件
        wrapper = queryBuilder.buildWrapper(query, wrapper);
        wrapper.function(funcCode).table(tableCode).selectColumns(param.getQueryColumns());


        return wrapper;
    }

    private void appendProjectInfo(ConditionsWrapper wrapper) {
        if (!"1".equals(systemVariableRpcService.requireSystemVariable("JE_PROJECT_MODEL_SWITCH"))) {
            return;
        }
        //如果开启了密级
        DynaBean tableBean = beanService.getResourceTable(wrapper.getTable());
        List<DynaBean> columns = tableBean.getDynaBeanList(BeanService.KEY_TABLE_COLUMNS);
        boolean isProjectTable = false;
        for (DynaBean eachColumn : columns) {
            if ("SY_PROJECT_ID".equals(eachColumn.getStr("TABLECOLUMN_CODE"))) {
                isProjectTable = true;
                break;
            }
        }
        if (isProjectTable) {
            wrapper.eq("SY_PROJECT_ID", ProjectContextHolder.getProject() == null ? null : ProjectContextHolder.getProject().getId());

            String currentOrgId = ProjectContextHolder.getProject().getOrgId();
            List<DynaBean> orgBeans = metaService.select("JE_PROJECT_ORG", ConditionsWrapper.builder()
                    .like("SY_PATH", currentOrgId));
            List<String> orgBeanIds = new ArrayList<>();
            for (DynaBean dynaBean : orgBeans) {
                orgBeanIds.add(dynaBean.getPkValue());
            }
            wrapper.in("SY_PROJECT_ORG_ID", orgBeanIds);
        }
    }

    private void appendSecretInfo(ConditionsWrapper wrapper) {
        if (!SystemSecretUtil.isOpen()) {
            return;
        }
        //如果开启了密级
        DynaBean tableBean = beanService.getResourceTable(wrapper.getTable());
        List<DynaBean> columns = tableBean.getDynaBeanList(BeanService.KEY_TABLE_COLUMNS);
        boolean secret = false;
        for (DynaBean eachColumn : columns) {
            if ("SY_SECRET_CODE".equals(eachColumn.getStr("TABLECOLUMN_CODE"))) {
                secret = true;
                break;
            }
        }
        if (secret) {
            List<String> secretCodeList = dataSecretRpcServive.requireContextDataSecretLevel();
            wrapper.in("SY_SECRET_CODE", secretCodeList);
        }
    }


}
