package com.je.demo.project.service;

import com.je.common.base.DynaBean;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.service.MetaService;
import com.je.common.base.service.PlatformService;
import com.je.rbac.rpc.RoleRpcService;
import com.je.servicecomb.CommonRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;

@Service
public class ProjectUserService implements CommonRequestService {

    @Autowired
    private MetaService metaService;
    @Autowired
    protected PlatformService manager;
    @Autowired
    protected RoleRpcService roleRpcService;

    @Transactional
    public DynaBean doSave(BaseMethodArgument param, HttpServletRequest request) {
        //获取角色信息，同步到rbac，角色表中
        String roleIds = request.getParameter("USER_JSID");
        String deptUserId = request.getParameter("USER_USERDEPT_ID");
        //同步劫色信息
        synchronizeRole(deptUserId, "", roleIds);
        return manager.doSave(param, request);
    }

    private void synchronizeRole(String accountDeptId, String originalRoleIds, String currentRoleIds) {
        roleRpcService.updateAccountRoles(accountDeptId, originalRoleIds, currentRoleIds);
    }

    @Transactional
    public DynaBean doUpdate(BaseMethodArgument param, HttpServletRequest request) {
        String roleIds = request.getParameter("USER_JSID");
        String deptUserId = request.getParameter("USER_USERDEPT_ID");
        DynaBean dynaBean = metaService.selectOneByPk("JE_PROJECT_USER", request.getParameter("JE_PROJECT_USER_ID"));
        //同步劫色信息
        synchronizeRole(deptUserId, dynaBean.getStr("USER_JSID"), roleIds);
        return manager.doUpdate(param, request);
    }

}
