package com.je.demo.project.controller;

import com.je.auth.check.annotation.AuthCheckDataPermission;
import com.je.auth.check.annotation.CheckDataType;
import com.je.common.base.DynaBean;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.demo.project.service.ProjectRoleService;
import com.je.demo.project.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/je/demo/project")
public class ProjectController extends AbstractPlatformController {

    @Autowired
    private ProjectService projectService;
    @Autowired
    private ProjectRoleService projectRoleService;


    @Override
    @RequestMapping(value = "/doSave", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @AuthCheckDataPermission(checkDataType = CheckDataType.UPDATE)
    public BaseRespResult doSave(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean dynaBean = manager.doSave(param, request);
        String projectId = dynaBean.getStr("");//获取主键
        String roleIds=""; //根据业务逻辑获取角色，用,号分割
        projectRoleService.batchImport(projectId, roleIds);
        return BaseRespResult.successResult(dynaBean);
    }

    @Override
    @RequestMapping(value = "/doUpdate", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult doUpdate(BaseMethodArgument param, HttpServletRequest request) {
        return BaseRespResult.successResult(projectService.doUpdate(param, request).getValues());
    }

    @Override
    @RequestMapping(value = "/doRemove", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult doRemove(BaseMethodArgument param, HttpServletRequest request) {
        return BaseRespResult.successResult(String.format("%s 条记录被删除", manager.doRemove(param, request)));
    }

}
