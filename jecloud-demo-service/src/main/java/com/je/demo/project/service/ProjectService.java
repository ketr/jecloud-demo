package com.je.demo.project.service;

import com.je.common.base.DynaBean;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import com.je.common.base.mapper.query.NativeQuery;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.service.CommonService;
import com.je.common.base.service.MetaResourceService;
import com.je.common.base.service.MetaService;
import com.je.common.base.service.rpc.BeanService;
import com.je.common.base.util.StringUtil;
import com.je.rbac.rpc.project.ProjectRoleRpcService;
import com.je.servicecomb.CommonRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;

@Service
public class ProjectService implements CommonRequestService {

    @Autowired
    private MetaService metaService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private ProjectRoleRpcService projectRoleRpcService;

    @Transactional
    public DynaBean doUpdate(BaseMethodArgument param, HttpServletRequest request) {
        //处理视图保存
        String funcCode = getStringParameter(request, "funcCode");
        //构建修改信息
        DynaBean dynaBean = (DynaBean) request.getAttribute("dynaBean");
        commonService.buildModelModifyInfo(dynaBean);

        DynaBean originDynaBean = metaService.selectOneByPk("JE_PROJECT_INFO",dynaBean.getStr("JE_PROJECT_INFO_ID"));
        if(dynaBean.containsKey("INFO_NAME") && !dynaBean.getStr("INFO_NAME").equals(originDynaBean.getStr("INFO_NAME"))){
            metaService.executeSql("UPDATE JE_PROJECT_ORG SET ORG_PROJECT_NAME = {0} WHERE JE_PROJECT_INFO_ID = {1}",
                    dynaBean.getStr("INFO_NAME"),dynaBean.getStr("JE_PROJECT_INFO_ID"));
            metaService.executeSql("UPDATE JE_PROJECT_USER SET USER_PROJECT_NAME = {0} WHERE JE_PROJECT_INFO_ID = {1}",
                    dynaBean.getStr("INFO_NAME"),dynaBean.getStr("JE_PROJECT_INFO_ID"));
            projectRoleRpcService.updateProjectRoleFolderName(dynaBean.getStr("JE_PROJECT_INFO_ID"),dynaBean.getStr("INFO_NAME"));
        }
        if (!commonService.validDynaBeanUpdatePerm(funcCode, dynaBean.getTableCode(), dynaBean.getPkCode(), dynaBean.getPkValue())) {
            throw new PlatformException("您没有此数据的更改权限！", PlatformExceptionEnum.UNKOWN_ERROR);
        }

        //处理单附件多附件上传
        commonService.doSaveFileMetadata(dynaBean, param.getBatchFilesFields(), param.getUploadableFields(), param.getFuncCode());
        metaService.update(dynaBean);

        return dynaBean;
    }

}
