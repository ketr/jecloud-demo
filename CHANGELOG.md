## 3.0.6 (2024-12-24)
### Features
- feat : 添加项目引用
- feat （修改pom）: 添加inspector引用
- feat:增加默认项目模式
- -feature:增加默认项目模式
- -feature:增加实时日志输出

## 3.0.5 (2024-10-30)
## 3.0.4 (2024-09-19)
## 3.0.3 (2024-07-26)
## 3.0.2 (2024-06-25)
### Features
- -feature:增加实时日志输出

## 3.0.1 (2024-06-01)
### Features
- feat （修改pom）: 添加inspector引用

## 3.0.0 (2024-04-25)
### Features
- -feature:增加证书控制，调整版本至3.0.0
- feat （案例）: 添加数据源引擎案例

## 2.2.3 (2024-02-23)
### Features
- -feature:抽离nacos
- -feature:适配nacos

## 2.2.2 (2023-12-29)
## 2.2.1 (2023-12-15)
### Features
- feat （oracle）: 添加oracle适配

## 2.2.0 (2023-11-15)
### Features
- feature : 添加示例demo
- -feature:修改README

## 2.1.1 (2023-10-27)
## 2.1.0 (2023-10-20)
## 2.0.9 (2023-09-22)
## 2.0.8 (2023-09-15)
### Features
- feature : 修改redis连接配置

## 1.4.1 (2023-09-08)